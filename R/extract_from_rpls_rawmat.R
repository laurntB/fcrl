
extract_from_rpls_rawmat <- function(data_rdrc)
{

  data_rdrc %>%
    mutate(
      finan_recode = case_when(
        FINAN == 10 & CONV == 1 ~ 'PLAI',
        FINAN == 10 & CONV == 2 ~ 'PLAI',
        FINAN == 11 & CONV == 1 ~ 'PLAI',
        FINAN == 11 & CONV == 2 ~ 'PLAI',
        FINAN == 12 & CONV == 1 ~ 'PLUS',
        FINAN == 12 & CONV == 2 ~ 'PLUS',
        FINAN == 13 & CONV == 1 ~ 'PLUS',
        FINAN == 13 & CONV == 2 ~ 'PLUS',
        FINAN == 14 & CONV == 1 ~ 'PLS',
        FINAN == 14 & CONV == 2 ~ 'PLS',
        FINAN == 15 & CONV == 1 ~ 'PLS',
        FINAN == 15 & CONV == 2 ~ 'PLS',
        FINAN == 16 & CONV == 1 ~ 'PLI',
        FINAN == 16 & CONV == 2 ~ 'PLI',
        FINAN == 17 & CONV == 1 ~ 'PLS',
        FINAN == 17 & CONV == 2 ~ 'PLI',
        FINAN == 49 & CONV == 1 ~ 'PLUS',
        FINAN == 49 & CONV == 2 ~ 'PLI',
        FINAN == 50 & CONV == 1 ~ 'PLUS',
        FINAN == 50 & CONV == 2 ~ 'PLUS',
        FINAN == 51 & CONV == 1 ~ 'PLUS',
        FINAN == 51 & CONV == 2 ~ 'PLUS',
        FINAN == 52 & CONV == 1 ~ 'PLUS',
        FINAN == 52 & CONV == 2 ~ 'PLUS',
        FINAN == 53 & CONV == 1 ~ 'PLUS',
        FINAN == 53 & CONV == 2 ~ 'PLUS',
        FINAN == 54 & CONV == 1 ~ 'PLUS',
        FINAN == 54 & CONV == 2 ~ 'PLI',
        FINAN == 55 & CONV == 1 ~ 'PLUS',
        FINAN == 55 & CONV == 2 ~ 'PLI',
        FINAN == 99 & CONV == 1 ~ 'PLUS',
        FINAN == 99 & CONV == 2 ~ 'PLI'
      ),
      finan_dom = case_when(
        finan_recode == 'PLAI' ~ 'LLTS',
        finan_recode == 'PLI'  ~ 'PLS',
        finan_recode == 'PLS'  ~ 'PLS',
        finan_recode == 'PLUS' ~ 'LLS'
      ),
      perim = as.character(DEPCOM)
    ) %>%
    select(
      perim,
      IDENT_REP,
      LOYERPRINC,
      EPCI,
      LIBEPCI,
      DEPCOM,
      LIBCOM,
      loymoy,
      NBPIECE,
      age,
      mes_sanscumul,
      QPV,
      finan_cus, # FINANAUTRE
      finan_recode,
      finan_dom
    ) %>%
    mutate(
      age.cls = case_when(
        age <= 5   ~ '0_5',
        age < 5    & age <= 10  ~ '5_10',
        age < 10   & age <= 20  ~ '10_20',
        age < 20   & age <= 40  ~ '20_40',
        age < 40   & age <= 60  ~ '40_60',
        age < 60                ~ '60_+',
      )
    ) %>%
    select(-age)

}
